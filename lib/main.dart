import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'weather_app.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.black,
          body: buildBody(),
          bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.black,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.map_outlined
                , color: Colors.white, size: 25,),
              label: '',
        
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.circle, color: Colors.white, size: 15,),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu, color: Colors.white, size: 25,),
              label: '',
            ),
          ],
        ),
        ),
      ),
    );
  }
}
